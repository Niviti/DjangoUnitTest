from django.test import TestCase
from django.utils.text import  slugify

from posts.models import Post

class PostModelTestCase(TestCase):

     def setUp(self):
         Post.objects.create(title="a New title", slug='some-prob-unique-slug-this-is-unique-123')

     def create_post(self, title='This title'):
          return Post.objects.create(title=title)

     def test_post_slug(self):

         title1 = 'another title abc'
         title2 = 'another title abc'
         slug1 = slugify(title1)
         slug2 = slugify(title2)
         obj1 = self.create_post(title=title1)
         obj2 = self.create_post(title=title2)
         self.assertEqual(obj1.slug, slug1)
         self.assertNotEqual(obj2.slug, slug2)

     def test_post_title(self):
         obj = Post.objects.get(slug='some-prob-unique-slug-this-is-unique-123')
         self.assertEqual(obj.title, 'a New title')
         self.assertTrue(obj.content == "") # if you want to change

     def test_post_queryset(self):
         title1 = "abc new school"
         obj = Post.objects.get(slug='some-prob-unique-slug-this-is-unique-123')
         self.assertEqual(obj.title, 'a New title')
         self.assertTrue(obj.content == "")  # if you want to change
